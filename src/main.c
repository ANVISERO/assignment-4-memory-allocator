#include "mem_internals.h"
#include "mem.h"
#include <sys/mman.h>


#define HEAP (2 * 4096)
#define TEST1 321
#define TEST2 321
#define TEST3 321
#define TEST4 ((2 * 4096) + 20)
#define TEST5 2048


void test_1() {
    printf("#########################################################\n");
    printf("<<<TEST №1>>>\n");
    printf("<<Normal successful memory allocation>>\n\n");
    void* heap = heap_init(HEAP);
    debug_heap(stdout, heap);
    if (heap == NULL) {
        fprintf(stderr, "TEST №1 FAILED: heap_init returned NULL!!!\n");
        return;
    }
    void* block = _malloc(TEST1);
    debug_heap(stdout, heap);
    if (block == NULL) { 
        fprintf(stderr, "TEST №1 FAILED: _malloc returned NULL!!!\n");
        return;
    }
    struct block_header* header = block_get_header(block);
    if (header == NULL) {
        fprintf(stderr, "TEST №1 FAILED: block_get_header returned NULL!!!\n");
        return;
    }
    if (header->is_free) { 
        fprintf(stderr, "TEST №1 FAILED: false did not set to is_free!!!\n");
        return;
    }
    if (header->capacity.bytes < HEAP && !header->next->is_free) {
        fprintf(stderr, "TEST №1 FAILED: blocks did not split!!!\n");
        return;
    }
    _free(block);
    debug_heap(stdout, heap);
    munmap(heap, size_from_capacity((block_capacity){.bytes = HEAP}).bytes);
    printf("\n<<<TEST №1 PASSED>>>\n\n");
}


void test_2() {
    printf("#########################################################\n");
    printf("<<<TEST №2>>>\n");
    printf("<<Freeing one block from several allocated blocks>>\n\n");
    void* heap = heap_init(HEAP);
    debug_heap(stdout, heap);
    if (heap == NULL) {
        fprintf(stderr, "TEST №2 FAILED: heap_init returned NULL!!!\n");
        return;
    }
    void* block_1 = _malloc(TEST2);
    void* block_2 = _malloc(TEST2);
    void* block_3 = _malloc(TEST2);
    debug_heap(stdout, heap);
    if (block_1 == NULL || block_2 == NULL || block_3 == NULL) { 
        fprintf(stderr, "TEST №2 FAILED: _malloc returned NULL!!!\n");
        return;
    }
    struct block_header* header_1 = block_get_header(block_1);
    struct block_header* header_2 = block_get_header(block_2);
    struct block_header* header_3 = block_get_header(block_3);
    debug_heap(stdout, heap);
    if (header_1 == NULL || header_2 == NULL || header_3 == NULL) {
        fprintf(stderr, "TEST №2 FAILED: block_get_header returned NULL!!!\n");
        return;
    }
    if (header_1->is_free || header_2->is_free || header_3->is_free) {
        fprintf(stderr, "TEST №2 FAILED: false did not set to is_free!!!\n");
        return;
    }
    if (header_1->capacity.bytes != TEST2 || header_2->capacity.bytes != TEST2 || header_3->capacity.bytes != TEST2) {
        fprintf(stderr, "TEST №2 FAILED: too little block was allocated!!!\n");
        return;
    }
    _free(block_2);
    debug_heap(stdout, heap);
    if (!header_2->is_free) { 
        fprintf(stderr, "TEST №2 FAILED: block_2 did not free!!!\n");
        return;
    }
    if (header_1->is_free || header_3->is_free) { 
        fprintf(stderr, "TEST №2 FAILED: block_1 or block_3 freed!!!\n");
        return;
    }
    _free(block_1);
    _free(block_3);
    debug_heap(stdout, heap);
    munmap(heap, size_from_capacity((block_capacity){.bytes = HEAP}).bytes);
    printf("\n<<<TEST №2 PASSED>>>\n\n");
}

void test_3() {
    printf("#########################################################\n");
    printf("<<<TEST №3>>>\n");
    printf("<<Freeing two blocks from several allocated>>\n\n");
    void* heap = heap_init(HEAP);
    debug_heap(stdout, heap);
    if (heap == NULL) {
        fprintf(stderr, "TEST №3 FAILED: heap_init returned NULL!!!\n");
        return;
    }
    void* block_1 = _malloc(TEST3);
    void* block_2 = _malloc(TEST3);
    void* block_3 = _malloc(TEST3);
    debug_heap(stdout, heap);
    if (block_1 == NULL || block_2 == NULL || block_3 == NULL) {
        fprintf(stderr, "TEST №3 FAILED: _malloc returned NULL!!!\n");
        return;
    }
    struct block_header* header_1 = block_get_header(block_1);
    struct block_header* header_2 = block_get_header(block_2);
    struct block_header* header_3 = block_get_header(block_3);
    debug_heap(stdout, heap);
    if (header_1 == NULL || header_2 == NULL || header_3 == NULL) {
        fprintf(stderr, "TEST №3 FAILED: block_get_header returned NULL!!!\n");
        return;
    }
    if (header_1->is_free || header_2->is_free || header_3->is_free) {
        fprintf(stderr, "TEST №3 FAILED: false did not set to is_free!!!\n");
        return;
    }
    if (header_1->capacity.bytes != TEST3 || header_2->capacity.bytes != TEST3 || header_3->capacity.bytes != TEST3) {
        fprintf(stderr, "TEST №3 FAILED: too little block was allocated!!!\n");
        return;
    }
    _free(block_2);
    _free(block_1);
    debug_heap(stdout, heap);
    if (!header_2->is_free) {
        fprintf(stderr, "TEST №3 FAILED: block_2 did not free!!!\n");
        return;
    }
    if (!header_1->is_free) {
        fprintf(stderr, "TEST №3 FAILED: block_1 did not free!!!\n");
        return;
    }
    if (header_3->is_free) {
        fprintf(stderr, "TEST №3 FAILED: block_3 freed!!!\n");
        return;
    }
    _free(block_3);
    debug_heap(stdout, heap);
    munmap(heap, size_from_capacity((block_capacity){.bytes = HEAP}).bytes);
    printf("\n<<<TEST №3 PASSED>>>\n\n");
}

void test_4() {
    printf("#########################################################\n");
    printf("<<<TEST №4>>>\n");
    printf("<<Memory ran out, the new memory region expands the old one>>\n\n");
    void* heap = heap_init(HEAP);
    debug_heap(stdout, heap);
    if (heap == NULL) {
        fprintf(stderr, "TEST №4 FAILED: heap_init returned NULL!!!\n");
        return;
    }
    void* block = _malloc(TEST4);
    debug_heap(stdout, heap);
    if (block == NULL) {
        fprintf(stderr, "TEST №4 FAILED: _malloc returned NULL!!!\n");
        return;
    }
    struct block_header* header = block_get_header(block);
    debug_heap(stdout, heap);
    if (header == NULL) {
        fprintf(stderr, "TEST №4 FAILED: block_get_header returned NULL!!!\n");
        return;
    }
    if (header->is_free) {
        fprintf(stderr, "TEST №4 FAILED: false did not set to is_free!!!\n");
        return;
    }
    if (header->capacity.bytes < TEST4) {
        fprintf(stderr, "TEST №4 FAILED: too little block was allocated!!!\n");
        return;
    }
    _free(block);
    debug_heap(stdout, heap);
    munmap(heap, size_from_capacity((block_capacity){.bytes = TEST4}).bytes);
    printf("\n<<<TEST №4 PASSED>>>\n\n");

}

void test_5() {
    printf("#########################################################\n");
    printf("<<<TEST №5>>>\n");
    printf("<<Adding memory when expansion is not possible>>\n\n");
    void* heap = heap_init(HEAP);
    debug_heap(stdout, heap);
    if (heap == NULL) {
        fprintf(stderr, "TEST №5 FAILED: heap_init returned NULL!!!\n");
        return;
    }
    void* block_1 = _malloc(HEAP);
    struct block_header* header_1 = block_get_header(block_1);
    debug_heap(stdout, heap);
    void* region = mmap((void *) header_1->contents + header_1->capacity.bytes,
                         REGION_MIN_SIZE,
                         PROT_READ | PROT_WRITE,
                         MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
    if (region == MAP_FAILED) {
        fprintf(stderr, "TEST №5 FAILED: MAP_FAILED!!!\n");
        return;
    }
    void* block_2 = _malloc(TEST5);
    if (block_2 == NULL) { 
        fprintf(stderr, "TEST №5 FAILED: _malloc returned NULL!!!\n");
        return;
    }
    struct block_header* header_2 = block_get_header(block_2);
    if (header_2 == NULL) {
        fprintf(stderr, "TEST №5 FAILED: block_get_header returned NULL!!!\n");
        return;
    }
    if (header_2->is_free) { 
        fprintf(stderr, "TEST №5 FAILED: false did not set to is_free!!!\n");
        return;
    }
    if (header_2->capacity.bytes < TEST5) { 
        fprintf(stderr, "TEST №5 FAILED: too little block was allocated!!!\n");
        return;
    }
    debug_heap(stdout, heap);
    _free(block_1);
    _free(block_2);
    debug_heap(stdout, heap);
    munmap(region, size_from_capacity((block_capacity){.bytes = REGION_MIN_SIZE}).bytes);
    munmap(heap, size_from_capacity((block_capacity){.bytes = HEAP}).bytes);
    printf("\n<<<TEST №5 PASSED>>>\n\n");
}


int main(void) {
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
    return 0;
}
